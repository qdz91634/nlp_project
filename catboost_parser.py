import pandas as pd
import numpy as np
import re
import nltk
import pymorphy2  

from catboost import CatBoostClassifier, cv, Pool

from nltk import tokenize
# from nltk import RegexpParser

from nltk.stem import SnowballStemmer
from nltk.corpus import stopwords

# from sklearn.metrics import f1_score, recall_score, precision_score, accuracy_score  # precision_recall_curve, auc
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer




stop_words = set(stopwords.words('russian'))
stemmer = SnowballStemmer('russian')
morph = pymorphy2.MorphAnalyzer()
tokenizer = tokenize.SpaceTokenizer()
parameters = {
        'depth': range(2, 10, 1),
        'n_estimators': range(60, 220, 40),
        'learning_rate': [0.1, 0.01, 0.001],
        'l2_leaf_reg': [3, 5, 9]}
chunk_patterns = {'name': r"""Chunk: {<PRP.?><VB.?><NN>?|<PRP.?><NN>?}""", 
                      'company': r"""Chunk: {<JJ.?><NN>?|<NN.?><NN>?|}"""}
path_table = '../train_test_data.xlsx'

def preprocess_vectorize(preprocess_df):
    preprocess_df['not_unk'] = preprocess_df.text_stem.apply(del_unk)
#     preprocess_df = preprocess_df[preprocess_df['not_unk'].apply(lambda x: x!=[])]
    return preprocess_df


def gridSearch_catb(parameters=parameters):
    cat_model = CatBoostClassifier(loss_function='MultiClass', random_seed=42)
    grid_search = cat_model.grid_search(parameters,
                                       X=X,
                                       y=y,
                                       plot=True,
                                       verbose=False)
    print(f'Лучшие параметры: {grid_search["params"]}')
    return grid_search["params"]


def cv_catb(pool, parameters, folds=3):
    
    return cv(params=parameters,
                pool=pool,
                fold_count=folds,
                stratified=True,
                verbose=False,
                return_models=True)
    

def calculate_metrics(pool, model, metrics_list=['AUC', 'Recall', 'Precision', 'Accuracy', 'F1']):
    metrics = model.eval_metrics(pool, 
                                     ntree_start=0, 
                                     ntree_end=0, 
                                     eval_period=1,
                          metrics=metrics_list)
    for key, value in {key: np.array(value).mean() for key, value in metrics.items()}.items():
        print(f'Значение метрики для класса {key}: {value}')

        
def yield_tokens(data_iter):
    for text, label in data_iter:
        yield text


def stratify_data(df_data, stratify_column_name, stratify_values, stratify_proportions, random_state=None):

    df_stratified = pd.DataFrame(columns = df_data.columns) # Create an empty DataFrame with column names matching df_data

    for i in range(len(stratify_values)): # iterate over the stratify values (e.g. "Male, Undergraduate" etc.)
        if i == len(stratify_values) - 1: 
            ratio_len = len(df_data) - len(df_stratified) # if this is the final iteration make sure we calculate the number of values for the last set such that the return data has the same number of rows as the source data
        else:
            ratio_len = int(len(df_data) * stratify_proportions[i]) # Calculate the number of rows to match the desired proportion

        df_filtered = df_data[df_data[stratify_column_name] == stratify_values[i]] # Filter the source data based on the currently selected stratify value
        
        try:
            df_temp = df_filtered.sample(replace=True, n=ratio_len, random_state=random_state) # Sample the filtered data using the calculated ratio

        except:
            df_temp = pd.DataFrame(columns=df_data.columns)
            
        df_stratified = pd.concat([df_stratified, df_temp]) # Add the sampled / stratified datasets together to produce the final result
        
    return df_stratified


def preprocess(path_table: str):
    try:
        df = pd.read_excel(path_table)
    except:
        df = pd.read_csv(path_table)
        
    # Делим данные на тренеровочную выборку и тестовую
    df_vocab = df.loc[lambda df: df.target.notnull()][(df['split']=='train')].drop_duplicates(subset=['text']).reset_index(drop=True)
    df_test = df.loc[lambda df: df.target.notnull()][df['split']=='test']
    
    # Токенизируем, отчищаем от стоп слов, и стемим каждый токен
    df_vocab['text_stem'] = df_vocab['text'].apply(lambda x: [stemmer.stem(y) if y not in stop_words else '' for y in tokenizer.tokenize(str(x).lower())])
    df_test['text_stem'] = df_test['text'].apply(lambda x: [stemmer.stem(y) if y not in stop_words else '' for y in tokenizer.tokenize(str(x).lower())])
    
    # Отчищаем от пустых строк и списков токенов
    df_vocab['text_stem'] = df_vocab['text_stem'].apply(lambda x: list(filter(None, x)))
    df_test['text_stem'] = df_test['text_stem'].apply(lambda x: list(filter(None, x)))

    df_vocab = df_vocab[df_vocab['text_stem'].apply(lambda x: x!=[])]
    df_test = df_test[df_test['text_stem'].apply(lambda x: x!=[])]
    
    # объединяем метки и стем-текст
    group_vocab = df_vocab.apply(lambda row: [row['text_stem'], int(row['target'])], axis=1)
    
    # Увеличиваем объём данных за счет бутсрепа
#     df_train = bootstrap_data(group_vocab, True, 500)
    
    X_test = df_test.apply(lambda row: [row['text_stem'], int(row['target'])], axis=1).tolist()

    # Стратифицируем данные
    stratify_values = [i for i in range(5)]
    stratify_proportions = [25, 25, 25, 25] #(np.array([100]) / 5).tolist() * 4
    df_stratified = stratify_data(df_vocab, 'target', stratify_values, stratify_proportions, random_state=42).reset_index(drop=True).sample(frac=1, random_state=42)
    X_stratified = df_stratified.apply(lambda row: [row['text_stem'], int(row['target'])], axis=1).to_list()
    
    return X_stratified, df_stratified, X_test, df_test, df_vocab, group_vocab


def train_catboost(X, y, X_test, y_test, parameters):
        

    #     Поиск нужных параметров по сетке
#         params = gridSearch_catb(parameters=parameters)
        
        params = {'depth': 6, 'l2_leaf_reg': 3, 
                 'iterations': 180, 
                 'learning_rate': 0.1, 
                 'loss_function': 'MultiClass', 
                 'random_seed': 42, 
                 'custom_metric': 'Accuracy'}
#         params['loss_function'] = 'MultiClass'
#         params['random_seed'] = 42
#         params['custom_metric'] = 'Accuracy'

        print(f'Словарь наилучших параметров {params}')
        cat_model = CatBoostClassifier(**params)

    #     кросс-валидация
        pool_cross_val = Pool(X, label=y)
        cv_data, cat_model_list = cv_catb(parameters=params, pool=pool_cross_val, folds=3)
#         print(f'Accuracy на стратифицированных данных при кросс-валидации: {cv_data["test-Accuracy-mean"].mean()}')
        
        cat_model = cat_model_list[-1]
    #     обучение и посчет метрик качества модели
#         cat_model.fit(X, y,
#                 eval_set = (X_test, y_test),
#                 verbose=False,
#                 plot=True)

        pool_test = Pool(X_test, label=y_test)
        metrics_list=['AUC', 'Recall', 'Precision', 'Accuracy', 'F1']
        calculate_metrics(pool=pool_test, model=cat_model, metrics_list=['AUC', 'Recall', 'Precision', 'Accuracy', 'F1']) 
        return cat_model
    
    
def del_unk(row):
    return list(filter(None, [token if token != '<unk>' else '' for token in row]))

def parse_text(target_list, df_, p=0.5):
    data_list = []
    df_[['label', 'sent', 'target_train', 'ngrams_train', 'cos_sim']] = None, None, None, None, None
    for target in target_list:
        df_target = df_[(df_['y_pred']==target)]
        for label, row in df_target[['text', 'text_stem']].iterrows():
            print(f'Анализ строки: |{row["text"]}|; класс |{target}|; номер строки: |{label}|')
            vector_test = vectorizer.transform([' '.join(row['text_stem'])])
            result = cosine_similarity(vector_test, X_vocab)

            if round(result.max(), 2) > p:
                df_target.loc[label, ['label', 'sent', 'target_train', 'ngrams_train', 'cos_sim']] = [label, 
                                                       row['text'],
                                                       df_vocab.iloc[result.argmax(), 5],
                                                       vectorizer.inverse_transform(X_vocab[result.argmax()]), 
                                                       result.max()]
            else:
                df_target.loc[label, ['label', 'sent', 'target_train', 'ngrams_train', 'cos_sim']] = [label, 
                                                      row['text'],
                                                       None,
                                                       None, 
                                                       None]

        data_list.append(df_target)
    
    return data_list


def change_tag(tag):
    if tag == 'NOUN':
        return 'NN'
    elif tag == 'VERB':
        return 'VB'
    elif tag in ['ADJF', 'ADJS']:
        return 'JJ'
    elif tag == 'NPRO':
        return 'PRP'

    
def process_content(func):
    def inner(chunkGram, text_present_list: list, flag_search: str):
        search_list = []
        for sent in text_present_list:
            sent = ' '.join([morph.parse(token.lower())[0].normal_form for token in tokenizer.tokenize(sent)])
#             print(sent)
            if flag_search == 'name':
                re_search = re.search(r'я\sзвать\s[а-я]+|я\s[а-я]+', sent)
            elif flag_search == 'company':
                re_search = re.search(r'(?<=компания)\s[а-я]+\s[а-я]*\s[а-я]*', sent)
                
            if re_search != None:
                print('поиск по паттерну: ', re_search.group())
                search_list.append(re_search.group())
            else:
                search_list.append('')
        return func(chunkGram, search_list, flag_search)
    return inner


@process_content
def search_by_tag(chunkGram, text_present_list: list,  flag_search: str):
    instance_list = []
    try:          
        for sent in text_present_list:
            print(sent)
            if sent == '':
                instance_list.append('')
            tagged_list = []
            for token in tokenizer.tokenize(sent):
                tag_pos = change_tag(morph.parse(token.lower())[0].tag.POS)
                if tag_pos == None:
                    continue
                tagged = morph.parse(token.lower())[0].normal_form, tag_pos
                tagged_list.append(tagged)

            chunkParser = nltk.RegexpParser(chunkGram)
            chunked = chunkParser.parse(tagged_list)

            for subtree in chunked.subtrees(filter=lambda t: t.label() == 'Chunk'):
                print(subtree.leaves())
                if flag_search == 'name':
                    instance_list.append(subtree.leaves()[-1][0])
                elif flag_search == 'company':
                    instance_list.append(' '.join([elem[0] for elem in subtree.leaves()]))
            
    except Exception as e:
        print(str(e))
    return instance_list
    

def rename_columns(list_columns, targets_list):
    return [list_columns[target] for target in targets_list]


def get_result(result_df, role: str):
    idx_list = ['idx_hello', 'idx_bye', 'idx_present']
    category_list = ['hello', 'bye', 'present']
    text_list = ['text_hello', 'text_bye', 'text_present']
    names_list = ['name', 'company']
    
    result_example = result_df.dropna(subset=['ngrams_train', 'cos_sim'])
    result_example['cos_sim'] = result_example['cos_sim'].apply(lambda row: 1 if row > 0.5 else 0)
    
    # display(result_example)
#     создаем pivot tables для результирующей тпблицы
    client_or_manager = result_example[result_example['role']==role]
    phrases = pd.pivot_table(client_or_manager, values='line_n', index=['dlg_id'],
                        columns=['role', 'target'])
    # display(phrases)
    print(phrases.shape)
    
    phrases.columns = [index for role, index in phrases.columns.tolist()]
    phrases_targets = phrases.columns.tolist()
    phrases_targets.sort()
    idx_columns = rename_columns(idx_list, 
                                     targets_list=phrases_targets)
    category_columns = rename_columns(category_list, 
                                     targets_list=phrases_targets)
    text_columns = rename_columns(text_list, 
                                     targets_list=phrases_targets)
       
    print(phrases.columns.tolist())
    print(idx_columns)
    print(phrases_targets)
    phrases.columns = idx_columns
    phrases[text_columns + names_list] = [''] * len(text_columns + names_list)
#     display(phrases)
#    вытягиваем реплики приветствия, прощания и предствалнеия по меткам и индексам строк
    phrases_list = []
    for label, row in phrases[idx_columns].iterrows():
        phrases_list.append([''.join(client_or_manager['text'][(client_or_manager['line_n'].isin([elem,])) & 
                                                      (client_or_manager['dlg_id']==label)].tolist()) \
                                                if elem != np.nan else np.nan for elem in row.tolist()])
    # print(phrases_list)
    phrases[text_columns] = phrases_list 
    
#     Вытягиваем имена людей и названия компаний
    if 'text_present' in text_columns:
        result_list = [search_by_tag(chunkGram, phrases['text_present'].tolist(), key) for key, chunkGram in chunk_patterns.items()]
#     print(result_list)    
        phrases[['name', 'company']] = np.array(result_list).T
#     display(phrases) 
    else:
        phrases[['name', 'company']] = np.zeros((phrases.shape[0], 2))
        
#     Определяем представился, и поприветствовал и попращался ли человек
    requirements = pd.pivot_table(client_or_manager, values='cos_sim', index=['dlg_id'],
                        columns=['role', 'target'], fill_value=0)
    
    
    requirements.columns = category_columns
    
    if 'hello' in category_columns and 'bye' in category_columns:
        requirements['hello_and_bye'] = requirements[['hello', 'bye']].sum(axis=1)
    elif 'hello' in category_columns:
        requirements['hello_and_bye'] = requirements['hello']
    elif 'bye' in category_columns:
        requirements['hello_and_bye'] = requirements['hello']
#    добавить итог в итоговую таблицу и соединить phrases и requirements 
    # display(requirements)
    return requirements.merge(phrases, left_index=True, right_index=True, how='inner')




if __name__ == '__main__':
    
    role = 'client'

#   Начинаем препроцессинг
    X_stratified, df_stratified, X_test, df_test, df_vocab, group_vocab = preprocess(path_table)
    
    #     Для обучение векторизатора
    df_vocab = preprocess_vectorize(df_vocab)
    corpus_vocab = df_vocab['not_unk'] \
                            .apply(lambda row: ' '.join(row)).tolist()
    
#     Для преобразования признаков в числовой вектор
    df_stratified = preprocess_vectorize(df_stratified)
    corpus_stratified = df_stratified['not_unk'] \
                            .apply(lambda row: ' '.join(row)).tolist()
    
#     Для преобразования признаков тестовой выборки в числовой вектор
    corpus_test = df_test['text_stem'] \
                            .apply(lambda row: ' '.join(row)).tolist()
    
    vectorizer = TfidfVectorizer(ngram_range=(1, 4),
                                 tokenizer=tokenizer.tokenize,
                                 stop_words=stop_words,
                                 max_df=0.6,
                                 min_df=0.2)
    X_vocab = vectorizer.fit_transform(corpus_vocab).todense()
    
# Подготавливаем данные для кросс-валидации
# train
    X = vectorizer.transform(corpus_stratified)
    y = df_stratified['target'].values

# test
    X_test = vectorizer.transform(corpus_test)
    y_test = df_test['target'].values
    
# Подготовка catboost к предикту
    cat_model = train_catboost(X, y, X_test, y_test, parameters)
    print('df_test', df_test.shape)
    print('X_test', X_test.shape)
    df_test['y_pred'] = cat_model.predict(X_test).argmax(-1)
    arr_predict = df_test['y_pred'].values
    mask = (y_test == arr_predict)
    print(f'Количество првильно проклассифицированных {y_test[mask].shape[0]} из {y_test.shape[0]}')
    
#     2 часть работа с парсером

    df_vocab['not_unk'] = df_vocab.text_stem.apply(del_unk)
    df_vocab = df_vocab[df_vocab['not_unk'].apply(lambda x: x!=[])]

    corpus = df_vocab['not_unk'].apply(lambda row: ' '.join(row)).tolist()

    print('Обучение векторизатора для парсера: ', corpus)
    
    vectorizer = TfidfVectorizer(ngram_range=(1, 4),
                                 tokenizer=tokenizer.tokenize,
                                 stop_words=stop_words)
    X_vocab = vectorizer.fit_transform(corpus).todense()

    # итерируемся по датафрейму с тестовыми данными
    # по косинусному определяем подходит есть ли схожие предложения с нашим паттерном

    data_list = parse_text([0, 1, 2], df_=df_test.copy(deep=True), p=0.5)
    df_cos_sim = pd.concat(data_list)
    
    df_cos_sim = df_cos_sim.loc[(df_cos_sim['ngrams_train']!='None') & 
                               (df_cos_sim['cos_sim']!='None')]
    
    df_other_test = df_test.copy(deep=True)
    result_2 = df_other_test.merge(df_cos_sim[['sent', 'target_train', 'ngrams_train', 'cos_sim']], 
                                 how='inner', 
                                 left_index=True, 
                                 right_index=True)
    
#     Готовим результирующую таблицу на выход
    result_2 = get_result(result_2, role)
    result_2.to_excel('../result_parse_2.xlsx')