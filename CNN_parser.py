import pandas as pd
import numpy as np
import re
import torch
import nltk
import pymorphy2  

from tqdm import tqdm

from torch import nn
from torch.utils.data import DataLoader

from nltk import tokenize
# from nltk import RegexpParser

from nltk.stem import SnowballStemmer
from nltk.corpus import stopwords

# from sklearn.metrics import f1_score, recall_score, precision_score, accuracy_score  # precision_recall_curve, auc
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer

from torchtext.vocab import build_vocab_from_iterator




nltk.download('stopwords')
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')



stop_words = set(stopwords.words('russian'))
stemmer = SnowballStemmer('russian')
morph = pymorphy2.MorphAnalyzer()
chunk_patterns = {'name': r"""Chunk: {<PRP.?><VB.?><NN>?|<PRP.?><NN>?}""", 
                  'company': r"""Chunk: {<JJ.?><NN>?|<NN.?><NN>?|}"""}
tokenizer = tokenize.SpaceTokenizer()
limit_tokens = 10 # df_vocab.text.apply(lambda row: len(row)).max()
path_table = '../train_test_data.xlsx'

def train(epochs,
          model, 
          optimizer, 
          criterion, 
          train_iterator, 
          valid_iterator, max_grad_norm=1):
    for epoch in tqdm(range(1, epochs + 1)):
        training_loss = 0.0
        valid_loss = 0.0
        model.train()
        for label, X in tqdm(train_iterator):
            optimizer.zero_grad()
            predict = model(X)
            loss = criterion(predict, label)
            loss.backward()
            if max_grad_norm is not None:
                torch.nn.utils.clip_grad_norm_(model.parameters(), max_grad_norm)
            optimizer.step()
            training_loss += loss
        training_loss /= len(train_iterator)
        
        correct = 0
        num_objs = 0
        list_predict = []
        labels_list = []
        model.eval()
        with torch.no_grad():
            for label, X in tqdm(valid_iterator):
                predict = model(X)
                loss = criterion(predict, label)
                valid_loss += loss
                correct += (label == predict.argmax(-1)).sum()
                num_objs += len(label)
                list_predict += predict.argmax(-1).numpy().tolist()
                labels_list += label.numpy().tolist()
            
        valid_loss /= len(valid_iterator)
        print('Epoch: {}, Training Loss: {:.3f}, Validation Loss: {:.3f}'.format(epoch, training_loss, valid_loss))
        print('Epoch: {}, accuracy score: {}'.format(epoch, correct/num_objs))
        if correct/num_objs >= 0.97:
            break
    return list_predict, labels_list


# def get_bootstrap_sample(x, B_sample=1):
#     N = x.size 
#     sample = np.random.choice(x, size=(N, B_sample), replace=True)
    
#     if B_sample == 1:
#         sample = sample.T[0]
#     return sample


# def bootstrap_data(data, bootstrap=False, B_sample=1):
    
#     if bootstrap:
#         data = get_bootstrap_sample(data, B_sample)
        
# #     X = np.vstack(data.tolist())
#     df_result = pd.DataFrame(np.concatenate(data), columns=['tuple',])
# #     print(df_result.columns)
#     df_result[['sent_stem', 'target']] = pd.DataFrame(df_result['tuple'].tolist(), index=df_result.index)
#     return df_result.drop('tuple', axis=1)


def yield_tokens(data_iter):
    for text, label in data_iter:
        yield text


def collate_batch(batch):
    label_list, text_list = [], []
    for _text, _label in batch:
        label_list.append(_label)
        text_list.append(vocab(_text))

    label_list = torch.tensor(label_list, dtype=torch.int64)
    text_list = torch.LongTensor([tokens+([0] * (limit_tokens-len(tokens))) 
                                                            if len(tokens)<limit_tokens else tokens[:limit_tokens] 
                                                                                 for tokens in text_list])
    return label_list.to(device), text_list.to(device)      
 

def stratify_data(df_data, stratify_column_name, stratify_values, stratify_proportions, random_state=None):

    df_stratified = pd.DataFrame(columns = df_data.columns) # Create an empty DataFrame with column names matching df_data

    for i in range(len(stratify_values)): # iterate over the stratify values (e.g. "Male, Undergraduate" etc.)
        if i == len(stratify_values) - 1: 
            ratio_len = len(df_data) - len(df_stratified) # if this is the final iteration make sure we calculate the number of values for the last set such that the return data has the same number of rows as the source data
        else:
            ratio_len = int(len(df_data) * stratify_proportions[i]) # Calculate the number of rows to match the desired proportion

        df_filtered = df_data[df_data[stratify_column_name] == stratify_values[i]] # Filter the source data based on the currently selected stratify value
        
        try:
            df_temp = df_filtered.sample(replace=True, n=ratio_len, random_state=random_state) # Sample the filtered data using the calculated ratio

        except:
            df_temp = pd.DataFrame(columns=df_data.columns)
            
        df_stratified = pd.concat([df_stratified, df_temp]) # Add the sampled / stratified datasets together to produce the final result
        
    return df_stratified


def preprocess(path_table: str):
    try:
        df = pd.read_excel(path_table)
    except:
        df = pd.read_csv(path_table)
        
    # Делим данные на тренеровочную выборку и тестовую
    df_vocab = df.loc[lambda df: df.target.notnull()][(df['split']=='train')].drop_duplicates(subset=['text']).reset_index(drop=True)
    df_test = df.loc[lambda df: df.target.notnull()][df['split']=='test']
    
    # Токенизируем, отчищаем от стоп слов, и стемим каждый токен
    df_vocab['text_stem'] = df_vocab['text'].apply(lambda x: [stemmer.stem(y) if y not in stop_words else '' for y in tokenizer.tokenize(str(x).lower())])
    df_test['text_stem'] = df_test['text'].apply(lambda x: [stemmer.stem(y) if y not in stop_words else '' for y in tokenizer.tokenize(str(x).lower())])
    
    # Отчищаем от пустых строк и списков токенов
    df_vocab['text_stem'] = df_vocab['text_stem'].apply(lambda x: list(filter(None, x)))
    df_test['text_stem'] = df_test['text_stem'].apply(lambda x: list(filter(None, x)))

    df_vocab = df_vocab[df_vocab['text_stem'].apply(lambda x: x!=[])]
    df_test = df_test[df_test['text_stem'].apply(lambda x: x!=[])]
    
    # объединяем метки и стем-текст
    group_vocab = df_vocab.apply(lambda row: [row['text_stem'], int(row['target'])], axis=1)
    
    # Увеличиваем объём данных за счет бутсрепа
#     df_train = bootstrap_data(group_vocab, True, 500)
    
    X_test = df_test.apply(lambda row: [row['text_stem'], int(row['target'])], axis=1).tolist()

    # Стратифицируем данные
    stratify_values = [i for i in range(5)]
    stratify_proportions = [25, 25, 25, 25] #(np.array([100]) / 5).tolist() * 4
    df_stratified = stratify_data(df_vocab, 'target', stratify_values, stratify_proportions, random_state=42).reset_index(drop=True).sample(frac=1, random_state=42)
    X_stratified = df_stratified.apply(lambda row: [row['text_stem'], int(row['target'])], axis=1).to_list()
    
    return X_stratified, df_stratified, X_test, df_test, df_vocab, group_vocab
    

def create_pipline(group_vocab):
    # Создаем словарь для определения каждой строке train датасета индексы для дальнейшего embedding'а
    vocab = build_vocab_from_iterator(yield_tokens(group_vocab.tolist()), specials=["<unk>"])
    # vocab.set_default_index(vocab["<mask>"])
    vocab.set_default_index(vocab["<unk>"])
    
    # Создаем pipline
    text_pipeline = lambda x: vocab(tokenizer.tokenize(x))
    label_pipeline = lambda x: int(x)
    
    return vocab, text_pipeline, label_pipeline 


# def lemmatize(row):
#     return ' '.join([morph.parse(token)[0].normal_form for token in tokenizer.tokenize(row)])

def del_unk(row):
    return list(filter(None, [token if token != '<unk>' else '' for token in row]))

def parse_text(target_list, df_, p=0.5):
    data_list = []
    df_[['label', 'sent', 'target_train', 'ngrams_train', 'cos_sim']] = None, None, None, None, None
    for target in target_list:
        df_target = df_[(df_['y_pred']==target)]
        for label, row in df_target[['text', 'text_stem']].iterrows():
            print(f'Анализ строки: |{row["text"]}|; класс |{target}|; номер строки: |{label}|')
            vector_test = vectorizer.transform([' '.join(row['text_stem'])])
            result = cosine_similarity(vector_test, X_vocab)

            if round(result.max(), 2) > p:
                df_target.loc[label, ['label', 'sent', 'target_train', 'ngrams_train', 'cos_sim']] = [label, 
                                                       row['text'],
                                                       df_vocab.iloc[result.argmax(), 5],
                                                       vectorizer.inverse_transform(X_vocab[result.argmax()]), 
                                                       result.max()]
            else:
                df_target.loc[label, ['label', 'sent', 'target_train', 'ngrams_train', 'cos_sim']] = [label, 
                                                      row['text'],
                                                       None,
                                                       None, 
                                                       None]

        data_list.append(df_target)
    
    return data_list


def change_tag(tag):
    if tag == 'NOUN':
        return 'NN'
    elif tag == 'VERB':
        return 'VB'
    elif tag in ['ADJF', 'ADJS']:
        return 'JJ'
    elif tag == 'NPRO':
        return 'PRP'

    
def process_content(func):
    def inner(chunkGram, text_present_list: list, flag_search: str):
        search_list = []
        for sent in text_present_list:
            sent = ' '.join([morph.parse(token.lower())[0].normal_form for token in tokenizer.tokenize(sent)])
#             print(sent)
            if flag_search == 'name':
                re_search = re.search(r'я\sзвать\s[а-я]+|я\s[а-я]+', sent)
            elif flag_search == 'company':
                re_search = re.search(r'(?<=компания)\s[а-я]+\s[а-я]*\s[а-я]*', sent)
                
            if re_search != None:
                print('поиск по паттерну: ', re_search.group())
                search_list.append(re_search.group())
            else:
                search_list.append('')
        return func(chunkGram, search_list, flag_search)
    return inner


@process_content
def search_by_tag(chunkGram, text_present_list: list,  flag_search: str):
    instance_list = []
    try:          
        for sent in text_present_list:
            print(sent)
            if sent == '':
                instance_list.append('')
            tagged_list = []
            for token in tokenizer.tokenize(sent):
                tag_pos = change_tag(morph.parse(token.lower())[0].tag.POS)
                if tag_pos == None:
                    continue
                tagged = morph.parse(token.lower())[0].normal_form, tag_pos
                tagged_list.append(tagged)

            chunkParser = nltk.RegexpParser(chunkGram)
            chunked = chunkParser.parse(tagged_list)

            for subtree in chunked.subtrees(filter=lambda t: t.label() == 'Chunk'):
                print(subtree.leaves())
                if flag_search == 'name':
                    instance_list.append(subtree.leaves()[-1][0])
                elif flag_search == 'company':
                    instance_list.append(' '.join([elem[0] for elem in subtree.leaves()]))
            
    except Exception as e:
        print(str(e))
    return instance_list
    

def rename_columns(list_columns, targets_list):
    return [list_columns[target] for target in targets_list]


def get_result(result_df, role: str):
    idx_list = ['idx_hello', 'idx_bye', 'idx_present']
    category_list = ['hello', 'bye', 'present']
    text_list = ['text_hello', 'text_bye', 'text_present']
    names_list = ['name', 'company']
    
    result_example = result_df.dropna(subset=['ngrams_train', 'cos_sim'])
    result_example['cos_sim'] = result_example['cos_sim'].apply(lambda row: 1 if row > 0.5 else 0)
    
    # display(result_example)
#     создаем pivot tables для результирующей тпблицы
    client_or_manager = result_example[result_example['role']==role]
    phrases = pd.pivot_table(client_or_manager, values='line_n', index=['dlg_id'],
                        columns=['role', 'target'])
    # display(phrases)
    print(phrases.shape)
    
    phrases.columns = [index for role, index in phrases.columns.tolist()]
    phrases_targets = phrases.columns.tolist()
    phrases_targets.sort()
    idx_columns = rename_columns(idx_list, 
                                     targets_list=phrases_targets)
    category_columns = rename_columns(category_list, 
                                     targets_list=phrases_targets)
    text_columns = rename_columns(text_list, 
                                     targets_list=phrases_targets)
       
    print(phrases.columns.tolist())
    print(idx_columns)
    print(phrases_targets)
    phrases.columns = idx_columns
    phrases[text_columns + names_list] = [''] * len(text_columns + names_list)
#     display(phrases)
#    вытягиваем реплики приветствия, прощания и предствалнеия по меткам и индексам строк
    phrases_list = []
    for label, row in phrases[idx_columns].iterrows():
        phrases_list.append([''.join(client_or_manager['text'][(client_or_manager['line_n'].isin([elem,])) & 
                                                      (client_or_manager['dlg_id']==label)].tolist()) \
                                                if elem != np.nan else np.nan for elem in row.tolist()])
    # print(phrases_list)
    phrases[text_columns] = phrases_list 
    
#     Вытягиваем имена людей и названия компаний
    if 'text_present' in text_columns:
        result_list = [search_by_tag(chunkGram, phrases['text_present'].tolist(), key) for key, chunkGram in chunk_patterns.items()]
#     print(result_list)    
        phrases[['name', 'company']] = np.array(result_list).T
#     display(phrases) 
    else:
        phrases[['name', 'company']] = np.zeros((phrases.shape[0], 2))
        
#     Определяем представился, и поприветствовал и попращался ли человек
    requirements = pd.pivot_table(client_or_manager, values='cos_sim', index=['dlg_id'],
                        columns=['role', 'target'], fill_value=0)
    
    
    requirements.columns = category_columns
    
    if 'hello' in category_columns and 'bye' in category_columns:
        requirements['hello_and_bye'] = requirements[['hello', 'bye']].sum(axis=1)
    elif 'hello' in category_columns:
        requirements['hello_and_bye'] = requirements['hello']
    elif 'bye' in category_columns:
        requirements['hello_and_bye'] = requirements['hello']
#    добавить итог в итоговую таблицу и соединить phrases и requirements 
    # display(requirements)
    return requirements.merge(phrases, left_index=True, right_index=True, how='inner')


class CNNModel(nn.Module):
    def __init__(self, embed_size, hidden_size, num_classes=4):
        super().__init__()
        self.embeddings = nn.Embedding(len(vocab), embedding_dim=embed_size, padding_idx=0)
        self.cnn = nn.Sequential(
            nn.Conv1d(embed_size, hidden_size, kernel_size=3, padding=1, stride=2),
            nn.BatchNorm1d(hidden_size),
            nn.LeakyReLU(negative_slope = 0.04),
            nn.Conv1d(hidden_size, hidden_size, kernel_size=3, padding=1, stride=2),
            nn.BatchNorm1d(hidden_size),
            nn.LeakyReLU(negative_slope = 0.04),
            nn.Conv1d(hidden_size, hidden_size, kernel_size=3, padding=1, stride=2),
            nn.BatchNorm1d(hidden_size),
            nn.Dropout(p=0.7),
            nn.LeakyReLU(negative_slope = 0.04),
            nn.AdaptiveMaxPool1d(1),
            nn.Flatten(),
        )
        self.cl = nn.Sequential(
            nn.Linear(hidden_size, num_classes)
        )

    def forward(self, x):
        x = self.embeddings(x)  # (batch_size, seq_len, embed_dim)
        x = x.permute(0, 2, 1)
        x = self.cnn(x)
        prediction = self.cl(x)
        return prediction







if __name__ == '__main__':
    
    # Основные экземпляры классов и данные для токенизации, очистки стоп-слов лемматизици и стемминга
    role = 'client'
    
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    
    # Проводим препроцессинг данных
    X_stratified, df_stratified, X_test, df_test, df_vocab, group_vocab = preprocess(path_table)
    vocab, text_pipeline, label_pipeline = create_pipline(group_vocab)

    train_iterator = DataLoader(X_stratified, batch_size=8, shuffle=False, collate_fn=collate_batch)
    test_iterator = DataLoader(X_test, batch_size=8, shuffle=False, collate_fn=collate_batch)
    
    # Создаем модель сверточнй нейронной сети, оптимизатор и функцию потерь(loss) 
    # на основе которой будем считать функционал ошибки
    device = "cuda" if torch.cuda.is_available() else "cpu"
    model = CNNModel(100, 220).to(device)
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), weight_decay=0.8, eps=1e-10, lr=6e-5)
    
#   Валидируем модель и предсказываем на тестовых данных
    epochs = 50
    list_predict, labels_list = train(epochs,
                                      model, 
                                      optimizer, 
                                      criterion, 
                                      train_iterator, 
                                      test_iterator)
    
    df_test['y_pred'] = list_predict
    # Смотрю сколько проклассифицировал правильно из 435 строк текста тестовых данных
    mask = (np.array(labels_list) == np.array(list_predict))
    print(f'Количество првильно проклассифицированных {np.array(labels_list)[mask].shape[0]} из {len(labels_list)}')
    
#     2 часть работа с парсером
    df_vocab['not_unk'] = df_vocab.text_stem.apply(del_unk)
    df_vocab = df_vocab[df_vocab['not_unk'].apply(lambda x: x!=[])]
    corpus = df_vocab['not_unk'].apply(lambda row: ' '.join(row)).tolist()

    print(corpus)
    
    vectorizer = TfidfVectorizer(ngram_range=(1, 4),
                                 tokenizer=tokenizer.tokenize,
                                 stop_words=stop_words)
    X_vocab = vectorizer.fit_transform(corpus).todense()

    # итерируемся по датафрейму с тестовыми данными
    # по косинусному определяем подходит есть ли схожие предложения с нашим паттерном

    data_list = parse_text([0, 1, 2], df_=df_test.copy(deep=True), p=0.5)
    df_cos_sim = pd.concat(data_list)
    
    df_cos_sim = df_cos_sim.loc[(df_cos_sim['ngrams_train']!='None') & 
                               (df_cos_sim['cos_sim']!='None')]
    
    df_other_test = df_test.copy(deep=True)
    result_1 = df_other_test.merge(df_cos_sim[['sent', 'target_train', 'ngrams_train', 'cos_sim']], 
                                 how='inner', 
                                 left_index=True, 
                                 right_index=True)
    
#     Готовим результирующую таблицу на выход
    result_1 = get_result(result_1, role)

    result_1.to_excel('../result_parse_1.xlsx')
    